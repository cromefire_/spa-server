package main

import (
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"os"
	"path"
	"regexp"
	"strings"
)

var fileReg = regexp.MustCompile("\\.[a-z]$")

func main() {
	router := mux.NewRouter()
	router.MatcherFunc(func(request *http.Request, match *mux.RouteMatch) bool {
		p := path.Join("app", request.URL.Path)
		_, err := os.Stat(p)
		return err != nil
	}).Handler(http.FileServer(http.Dir("app")))
	router.MatcherFunc(func(request *http.Request, match *mux.RouteMatch) bool {
		accept := request.Header.Get("Accept")
		if !strings.Contains(accept, "text/html") {
			return false
		}
		if fileReg.MatchString(request.URL.Path) {
			return false
		}
		return true
	}).HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
		http.ServeFile(writer, request, "app/index.html")
	})
	log.Print("Listening on: http://localhost:8097")
	err := http.ListenAndServe(":8097", router)
	if err != nil {
		log.Fatal(err)
	}
}
